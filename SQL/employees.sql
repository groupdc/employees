/*
Navicat MySQL Data Transfer

Source Server         : Combell_linux
Source Server Version : 50505
Source Host           : 83.217.73.50:3306
Source Database       : deceuster_new

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-02-13 14:15:10
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `employees`
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newemployee` tinyint(4) DEFAULT '0',
  `enabletime` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `birthdaycycle` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `highpriority` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `enablemorebtn` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `online` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `actionemployee` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `calendar` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `archive` tinyint(4) DEFAULT '0',
  `thumbnailindetail` tinyint(4) DEFAULT '0' COMMENT 'custom dcm',
  `deleted` tinyint(4) DEFAULT '0',
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `i_Highpriority` (`highpriority`),
  KEY `i_Enabletime` (`enabletime`),
  KEY `i_startdate` (`startdate`),
  KEY `i_enddate` (`enddate`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for `employees_categories_language`
-- ----------------------------
DROP TABLE IF EXISTS `employees_categories_language`;
CREATE TABLE `employees_categories_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for `employees_language`
-- ----------------------------
DROP TABLE IF EXISTS `employees_language`;
CREATE TABLE `employees_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(11) unsigned DEFAULT '1',
  `employee_category_id` int(11) unsigned DEFAULT NULL,
  `employee_id` int(11) unsigned DEFAULT NULL,
  `sort_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `body` text COLLATE utf8_unicode_ci,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'custom dcm - extra url to image or external doc/link',
  `url_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `_oldemployeeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_employeelanguage` (`language_id`),
  KEY `FK_categoryid` (`employee_category_id`),
  KEY `FK_employeeid` (`employee_id`),
  FULLTEXT KEY `EmployeeSearchHelper` (`title`,`description`),
  CONSTRAINT `employees_language_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `employees_language_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `employees_language_ibfk_3` FOREIGN KEY (`employee_category_id`) REFERENCES `employees_categories_language` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for `employees_language_to_pages`
-- ----------------------------
DROP TABLE IF EXISTS `employees_language_to_pages`;
CREATE TABLE `employees_language_to_pages` (
  `employee_detail_id` int(11) unsigned DEFAULT NULL,
  `page_id` int(11) unsigned DEFAULT NULL,
  KEY `FK_employeedetail_id` (`employee_detail_id`),
  KEY `FK_pageid` (`page_id`),
  CONSTRAINT `employees_language_to_pages_ibfk_1` FOREIGN KEY (`employee_detail_id`) REFERENCES `employees_language` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `employees_language_to_pages_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `pages_language` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `employees_to_employees`
-- ----------------------------
DROP TABLE IF EXISTS `employees_to_employees`;
CREATE TABLE `employees_to_employees` (
  `employee_id` int(11) unsigned NOT NULL DEFAULT '0',
  `employee_tag_id` int(11) unsigned NOT NULL DEFAULT '0',
  KEY `FK_employee_id2` (`employee_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- View structure for `vwemployees`
-- ----------------------------
DROP VIEW IF EXISTS `vwemployees`;
CREATE ALGORITHM=UNDEFINED DEFINER=`marketing`@`94.107.236.219` SQL SECURITY DEFINER VIEW `vwemployees` AS select `employees`.`id` AS `id`,`employees`.`startdate` AS `startdate`,`employees`.`enddate` AS `enddate`,`employees`.`thumbnail` AS `thumbnail`,`employees`.`newemployee` AS `newemployee`,`employees`.`enabletime` AS `enabletime`,`employees`.`birthdaycycle` AS `birthdaycycle`,`employees`.`highpriority` AS `highpriority`,`employees`.`enablemorebtn` AS `enablemorebtn`,`employees`.`online` AS `online`,`employees`.`actionemployee` AS `actionemployee`,`employees`.`calendar` AS `calendar`,`employees`.`thumbnailindetail` AS `thumbnailindetail`,`employees`.`deleted` AS `deleted`,`employees_language`.`id` AS `employees_language_id`,`employees_language`.`language_id` AS `language_id`,`employees_language`.`employee_id` AS `employee_id`,`employees_language`.`sort_id` AS `sort_id`,`employees_language`.`title` AS `title`,`employees_language`.`description` AS `description`,`employees_language`.`body` AS `body`,`employees_language`.`url` AS `url`,`employees_language`.`url_slug` AS `url_slug`,`employees_language`.`url_path` AS `url_path`,`employees_categories_language`.`id` AS `employee_category_id`,`employees_categories_language`.`title` AS `employee_category`,`employees`.`created_at` AS `created_at`,`employees`.`updated_at` AS `updated_at` from ((`employees_language` join `employees` on((`employees_language`.`employee_id` = `employees`.`id`))) join `employees_categories_language` on(((`employees_language`.`employee_category_id` = `employees_categories_language`.`id`) and (`employees_categories_language`.`language_id` = `employees_language`.`language_id`)))) where (`employees`.`deleted` = 0);

-- ----------------------------
-- View structure for `vwemployees_page`
-- ----------------------------
DROP VIEW IF EXISTS `vwemployees_page`;
CREATE ALGORITHM=UNDEFINED DEFINER=`marketing`@`94.107.236.219` SQL SECURITY DEFINER VIEW `vwemployees_page` AS select `pages_language`.`id` AS `id`,`pages_language`.`language_id` AS `language_id`,`pages_language`.`parent_id` AS `parent_id`,`pages_language`.`lft` AS `lft`,`pages_language`.`rgt` AS `rgt`,`pages_language`.`depth` AS `depth`,`pages_language`.`title` AS `page_title`,`pages_language`.`body` AS `page_body`,`pages_language`.`thumbnail` AS `page_thumbnail`,`pages_language`.`url_path` AS `page_url_path`,`pages_language`.`url_slug` AS `page_url_slug`,`languages`.`language` AS `language`,`languages`.`country` AS `country`,`employees`.`startdate` AS `startdate`,`employees`.`enddate` AS `enddate`,`employees`.`thumbnail` AS `thumbnail`,`employees`.`newemployee` AS `newemployee`,`employees`.`enabletime` AS `enabletime`,`employees`.`birthdaycycle` AS `birthdaycycle`,`employees`.`highpriority` AS `highpriority`,`employees`.`enablemorebtn` AS `enablemorebtn`,`employees`.`online` AS `online`,`employees`.`actionemployee` AS `actionemployee`,`employees`.`calendar` AS `calendar`,`employees`.`thumbnailindetail` AS `thumbnailindetail`,`employees`.`deleted` AS `deleted`,`employees_language`.`employee_id` AS `employee_id`,`employees_language`.`sort_id` AS `sort_id`,`employees_language`.`title` AS `title`,`employees_language`.`description` AS `description`,`employees_language`.`body` AS `body`,`employees_language`.`url` AS `url`,`employees_language`.`url_slug` AS `url_slug`,`employees_language`.`url_path` AS `url_path` from ((((`pages_language` join `languages` on((`pages_language`.`language_id` = `languages`.`id`))) join `employees_language_to_pages` on((`employees_language_to_pages`.`page_id` = `pages_language`.`id`))) join `employees_language` on(((`employees_language_to_pages`.`employee_detail_id` = `employees_language`.`id`) and (`employees_language`.`language_id` = `languages`.`id`)))) join `employees` on((`employees_language`.`employee_id` = `employees`.`id`)));

-- ----------------------------
-- View structure for `vwemployees_to_employees`
-- ----------------------------
DROP VIEW IF EXISTS `vwemployees_to_employees`;
CREATE ALGORITHM=UNDEFINED DEFINER=`marketing`@`94.107.236.219` SQL SECURITY DEFINER VIEW `vwemployees_to_employees` AS select `employees_to_employees`.`employee_id` AS `employee_id`,`employees_to_employees`.`employee_tag_id` AS `employee_tag_id` from `employees_to_employees` union select `employees_to_employees`.`employee_tag_id` AS `employee_tag_id`,`employees_to_employees`.`employee_id` AS `employee_id` from `employees_to_employees`;
