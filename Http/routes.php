<?php
Route::group(['middleware' => ['web']], function () {
    Route::group(array("prefix" => "admin", "as"=>"admin."), function () {
        Route::group(['middleware' => 'auth:dcms'], function () {

                //EMPLOYEES
            Route::group(array("prefix" => "employees" ,"as"=>"employees."), function () {
                Route::get('{id}/copy', array('as'=>'copy', 'uses' => 'EmployeeController@copy'));
                Route::get('employeerelation/{employee_id?}', array('as'=>'employeerelation', 'uses' => 'EmployeeController@getEmployeeRelation'));

                //CATEGORIES
                Route::group(array("prefix" => "categories", "as"=>"categories."), function () {
                    Route::get('{id}/copy', array('as'=>'{id}.copy', 'uses' => 'CategoryController@copy'));
                    Route::any('api/table', array('as'=>'api.table', 'uses' => 'CategoryController@getDatatable'));
                });
                Route::resource('categories', 'CategoryController');

                //API
                Route::group(array("prefix" => "api", "as"=>"api."), function () {
                    Route::any('table', array('as'=>'table', 'uses' => 'EmployeeController@getDatatable'));
                });
            });
            Route::resource('employees', 'EmployeeController');
        });
    });
});
