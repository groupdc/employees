<?php

namespace Dcms\Employees\Http\Controllers;

use Dcms\Employees\Models\Employee;
use Dcms\Employees\Models\EmployeeToEmployee;
use Dcms\Employees\Models\Detail;
use Dcms\Employees\Models\Category;

//use Dcweb\Dcms\Models\Pages\Page;
//use Dcweb\Dcms\Controllers\Pages\PageController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;
use Form;
use DateTime;

class EmployeeController extends Controller
{
    public $employee_language_Columnnames = [];
    public $employee_Columnnames = [];

    public $employeelanguageFormTemplate = "";
    public $employeeFormTemplate = "";

    public $extendgeneralTemplate = "";

    public function __construct()
    {
        $this->middleware('permission:employees-browse')->only('index');
        $this->middleware('permission:employees-add')->only(['create', 'store']);
        $this->middleware('permission:employees-edit')->only(['edit', 'update']);
        $this->middleware('permission:employees-delete')->only('destroy');

        //note startdate and enddate are defaults, since these are always there, and have a specific value (not simple text, or bool)
        $this->employee_Columnnames = [
            'thumbnail'     => 'thumbnail',
            'online'        => 'online',
            'newemployee'    => 'newemployee',
        ];

        $this->employee_language_Columnnames = [
            'employee_category_id' => 'category_id',
            'title'               => 'title',
            'description'         => 'description',
            'sort_id'         => 'sort_id',
            'body'                => 'body',
            'slug'                => 'title',
            'path'                => 'title',
            'url'                 => 'url',
        ];

        $this->employeelanguageFormTemplate = null;
        $this->employeeFormTemplate = null;
    }

    public static function getEmployeesDetailByLanguage($language_id = null)
    {
        return Detail::where("language_id", "=", $language_id)->lists("title", "id");
    }

    public static function getDropdownEmployeesByLanguage($language_id = null)
    {
        $dropdownvalues = EmployeeController::getEmployeesDetailByLanguage($language_id);

        return Form::select("employee[" . $language_id . "][]", $dropdownvalues, [3, 4, 5, 6], ["id" => "employees-" . $language_id, "multiple" => "multiple"]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view
        return View::make('dcms::employees/index');
    }


    public function getDatatable()
    {
        return DataTables::queryBuilder(
            DB::connection('project')
                ->table('employees')
                ->select(
                    'employees.id',
                    'employees_language.title',
                    'employees_language.id as employee_language_id',
                    'employees_categories_language.title as category',
                    (DB::connection("project")->raw('Concat("<img src=\'/packages/dcms/core/images/flag-",lcase(country),".png\' >") as country'))
                )
                ->join('employees_language', 'employees.id', '=', 'employees_language.employee_id')
                ->leftJoin('employees_categories_language', 'employees_categories_language.id', '=', 'employees_language.employee_category_id')
                ->leftJoin('languages', 'employees_language.language_id', '=', 'languages.id')
        )
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/employees/' . $model->employee_language_id . '" accept-charset="UTF-8" class="pull-right">
								<input name="_token" type="hidden" value="' . csrf_token() . '">
								<input name="_method" type="hidden" value="DELETE">';
                if (Auth::user()->can('employees-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/employees/' . $model->id . '/edit"><i class="far fa-pencil"></i></a>';
                }
                if (Auth::user()->can('employees-add')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/employees/' . $model->employee_language_id . '/copy"><i class="far fa-copy"></i></a>';
                }
                if (Auth::user()->can('employees-delete')) {
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this employee" onclick="if(!confirm(\'Are you sure to delete this item ? \')){return false;};"><i class="far fa-trash-alt"></i></button>';
                }
                $edit .= '</form>';

                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    public function getInformation($id = null)
    {
        if (is_null($id)) {
            return DB::connection("project")->table("languages")->select((DB::connection("project")->raw("'' as title, NULL as sort_id, (select max(sort_id) from employees_language where language_id = languages.id) as maxsort, '' as description , '' as body, '' as url, '' as employee_category_id, '' as id")), "id as language_id", "language", "country", "language_name")->get();
        } else {
            return DB::connection("project")->select('
													SELECT language_id, languages.language, languages.country, languages.language_name, employee_category_id, employees_language.id, employee_id, title, sort_id, (select max(sort_id) from employees_language as X  where X.language_id = employees_language.language_id) as maxsort,  description, body, url, date_format(startdate,\'%d-%m-%Y\') as startdate , date_format(enddate,\'%d-%m-%Y\')  as enddate
													FROM employees_language
													LEFT JOIN languages on languages.id = employees_language.language_id
													LEFT JOIN employees on employees.id = employees_language.employee_id
													WHERE  languages.id is not null AND  employee_id = ?
													UNION
													SELECT languages.id , language, country, language_name, \'\' , \'\' ,  \'\' , \'\' , NULL as sort_id, (select max(sort_id) from employees_language where language_id = languages.id) as maxsort, \'\' , \'\'  , \'\' , \'\' , \'\'
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM employees_language WHERE employee_id = ?) ORDER BY 1
													', [$id, $id]);
        }
    }

    public function getExtendedModel()
    {
        //do nothing let the extend class hook into this
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languages = $this->getInformation();

        // load the create form (app/views/employees/create.blade.php)
        return View::make('dcms::employees/form')
            ->with('languages', $languages)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('pageOptionValuesSelected', [])
            ->with('sortOptionValues', $this->getSortOptions($languages, 1))
            ->with('employeelanguageFormTemplate', $this->employeelanguageFormTemplate)
            ->with('employeeFormTemplate', $this->employeeFormTemplate)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        $cat = $employee->category;

        // show the view and pass the nerd to it
        return View::make('dcms::employees/show')
            ->with('employee', $employee)
            ->with("category", $cat->title);
    }

    public function getSortOptions($model, $setExtra = 0)
    {
        foreach ($model as $M) {
            $increment = 0;
            if ($setExtra > 0) {
                $increment = $setExtra;
            }
            if (intval($M->id) <= 0 && !is_null($M->maxsort)) {
                $increment = 1;
            }

            $maxSortID = $M->maxsort;
            if (is_null($maxSortID)) {
                $maxSortID = 1;
            }

            for ($i = 1; $i <= ($maxSortID + $increment); $i++) {
                $SortOptions[$M->language_id][$i] = $i;
            }
        }

        return $SortOptions;
    }

    public function getSelectedPages($employeeid = null)
    {
        return DB::connection("project")->select('  SELECT employee_detail_id, page_id
													FROM employees_language_to_pages
													WHERE employee_detail_id IN (SELECT id FROM employees_language WHERE employee_id = ?)', [$employeeid]);
    }

    public function setPageOptionValues($objselected_pages)
    {
        $pageOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $pageOptionValuesSelected[$obj->page_id] = $obj->page_id;
            }
        }

        return $pageOptionValuesSelected;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $employee->startdate = (is_null($employee->startdate) ? null : DateTime::createFromFormat('Y-m-d', $employee->startdate)->format('m/d/Y'));
        $employee->enddate = (is_null($employee->enddate) ? null : DateTime::createFromFormat('Y-m-d', $employee->enddate)->format('m/d/Y'));

        $objlanguages = $this->getInformation($id);
        $objselected_pages = $this->getSelectedPages($id);

        return View::make('dcms::employees/form')
            ->with('employee', $employee)
            ->with('languages', $objlanguages)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('pageOptionValuesSelected', $this->setPageOptionValues($objselected_pages))
            ->with('sortOptionValues', $this->getSortOptions($objlanguages))
            ->with('employeelanguageFormTemplate', $this->employeelanguageFormTemplate)
            ->with('employeeFormTemplate', $this->employeeFormTemplate)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()]);
    }

    /**
     * copy the model
     *
     * @param  int $id
     *
     * @return Response
     */
    public function copy($id)
    {
        $newDetail = Detail::find($id)->replicate();
        $Employee = new Employee();
        $Employee->save();
        $newDetail->employee_id = $Employee->id;
        $newDetail->save();

        return Redirect::to('admin/employees');
    }


    private function validateEmployeeForm()
    {
        $rules = [
            //	'title' => 'required'
        ];
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()//to('admin/products/' . $id . '/edit')
            ->withErrors($validator)
                ->withInput();
        } else {
            return true;
        }
    }


    private function saveEmployeeProperties(Request $request, $employeeid = null)
    {
        // do check if the given id is existing.
        if (!is_null($employeeid) && intval($employeeid) > 0) {
            $Employee = Employee::find($employeeid);
        }

        if (!isset($Employee) || is_null($Employee)) {
            $Employee = new Employee;
        }

        foreach ($request->get("title") as $language_id => $title) {
            if (strlen(trim($request->get("title")[$language_id])) > 0 || strlen(trim($request->get("description")[$language_id])) > 0 || strlen(trim($request->get("body")[$language_id])) > 0) {
                //----------------------------------------
                // once there is a title set
                // we can set the properties to the employee
                // and instantly return the employee model
                // by default nothing is return so the
                // script may stop.
                //----------------------------------------

                foreach ($this->employee_Columnnames as $column => $inputname) {
                    if ($request->has($inputname)) {
                        $Employee->$column = $request->get($inputname);
                    } else {
                        $Employee->$column = null;
                    }
                }

                //$Employee->admin =  Auth::dcms()->user()->username;
                $Employee->save();

                return $Employee;
                break; // we only have to save the global settings once.
            }
        }
        //does not return anything by defuault... (may be false dunno - find out)
    }

    private function saveEmployeeDetail(Request $request, Employee $Employee, $givenlanguage_id = null)
    {
        $input = $request->all();

        $Detail = null;

        foreach ($input["title"] as $language_id => $title) {
            if (strlen(trim($input["title"][$language_id])) > 0 || strlen(trim($input["description"][$language_id])) > 0 || strlen(trim($input["body"][$language_id])) > 0) {
                if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id))) {
                    $Detail = null;
                    $newInformation = true;
                    $Detail = Detail::find($input["employee_information_id"][$language_id]);

                    if (is_null($Detail) === true) {
                        $Detail = new Detail();
                    } else {
                        $newInformation = false;
                    }

                    $oldSortID = null;
                    if ($newInformation == false && !is_null($Detail->sort_id) && intval($Detail->sort_id) > 0) {
                        $oldSortID = intval($Detail->sort_id);
                    }

                    // oldSortID > newID :: where sortid >= newid +1
                    if(intval($input["sort_id"][$language_id])>0 && $newInformation == false) {
                        if(intval($input["sort_id"][$language_id])>intval($oldSortID)) {
                            Detail::where('employee_category_id',$input[$this->employee_language_Columnnames["employee_category_id"]][$language_id])
                                    ->where('sort_id','>=',intval($oldSortID))
                                    ->where('sort_id', '<=',intval($input["sort_id"][$language_id]) )
                                    ->where('id','<>',$Detail->id)
                                    ->decrement('sort_id');
                        } elseif(intval($input["sort_id"][$language_id])<intval($oldSortID)) {
                            Detail::where('employee_category_id',$input[$this->employee_language_Columnnames["employee_category_id"]][$language_id])
                                    ->where('sort_id','>=',intval($input["sort_id"][$language_id]))
                                    ->where('sort_id','<=',intval($oldSortID))
                                    ->where('id','<>',$Detail->id)
                                    ->increment('sort_id');
                        }
                    }
                    if($newInformation == true){
                        Detail::where('employee_category_id',$input[$this->employee_language_Columnnames["employee_category_id"]][$language_id])
                        ->where('sort_id','>=',intval($input["sort_id"][$language_id]))
                        ->increment('sort_id');
                    }

                    $Detail->language_id = $language_id;

                    foreach ($this->employee_language_Columnnames as $column => $inputname) {
                        if (!in_array($column, ["slug", "path"])) {
                            $Detail->$column = $input[$inputname][$language_id];
                        }
                    }
                    $Detail->employee_category_id = ($input[$this->employee_language_Columnnames["employee_category_id"]][$language_id] == 0 ? null : $input[$this->employee_language_Columnnames["employee_category_id"]][$language_id]);
                    $Detail->url_slug = Str::slug($input[$this->employee_language_Columnnames["slug"]][$language_id]);
                    $Detail->url_path = Str::slug($input[$this->employee_language_Columnnames["path"]][$language_id]);

                    $Detail->save();
                    $Employee->detail()->save($Detail);

                    //----------------------------------------
                    // Detach the $Detail from all pages
                    //----------------------------------------
                    $Detail->pages()->detach();

                    //----------------------------------------
                    // link the employee to the selected page
                    // we will take the $Detail->id since
                    // this directly holds the language_id
                    // otherwise we'd be storing it twice
                    //----------------------------------------
                    if (isset($input["page_id"]) && isset($input["page_id"][$language_id]) && count($input["page_id"][$language_id]) > 0) {
                        foreach ($input["page_id"][$language_id] as $arrayindex => $page_id) {
                            $Detail->pages()->attach($page_id);
                        }
                    }
                }
            }
        }

        return $Detail;
    }

    public function getSelectedProductsInformation($employee_id = null)
    {
        return DB::connection("project")->select('	SELECT products_information_id, employees_language_id
													FROM employees_language_to_products_information
													WHERE employees_language_id IN (SELECT id FROM employees_language WHERE employee_id = ?)', [$employee_id]);
    }

    public function setProductsInformationOptionValues($objselected_pages)
    {
        $ProductsOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $ProductsOptionValuesSelected[$obj->products_information_id] = $obj->products_information_id;
            }
        }

        return $ProductsOptionValuesSelected;
    }

    public function saveEmployeeToProductInformation(Employee $Employee)
    {/*
        $input = $request->get();

        $Employee->productinformation()->detach();
        if(isset($input["information_group_id"]) && count($input["information_group_id"])>0)
        {
            foreach($input["information_group_id"] as $i => $information_group_id) $Employee->productinformation()->attach($information_group_id,array('information_group_id'=>$information_group_id));
        }*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->validateEmployeeForm() === true) {
            $Employee = $this->saveEmployeeProperties($request);

            if (!is_null($Employee)) {
                $this->saveEmployeeDetail($request, $Employee);
                $this->saveEmployeetToEmployee($request, $Employee);
            }

            // redirect
            Session::flash('message', 'Successfully created employee!');

            return Redirect::to('admin/employees');
        } else {
            return $this->validateEmployeeForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validateEmployeeForm() === true) {
            $Employee = $this->saveEmployeeProperties($request, $id);

            if (!is_null($Employee)) {
                $this->saveEmployeeDetail($request, $Employee);
                $this->saveEmployeetToEmployee($request, $Employee);
            }

            // redirect
            Session::flash('message', 'Successfully updated employee!');
            return Redirect::to('admin/employees');
        } else {
            return $this->validateEmployeeForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $Detail = Detail::find($id);
        EmployeeToEmployee::where('employee_id', '=', $id)->orWhere('employee_tag_id', '=', $id)->delete();

        $mainEmployeeID = $Detail->employee_id;

        //find the sortid of this product... so all the above can descent by 1
        $updateInformations = Detail::where('language_id', '=', $Detail->language_id)->get(['id', 'sort_id']);
        if (isset($updateInformations) && count($updateInformations) > 0) {
            foreach ($updateInformations as $uInformation) {
                $uInformation->sort_id = intval($uInformation->sort_id) - 1;
                $uInformation->save();
            }//end foreach($updateInformations as $Information)
        }//end 	if (count($updateInformations)>0)

        $Detail->delete();

        if (Detail::where("employee_id", "=", $mainEmployeeID)->count() <= 0) {
            Employee::destroy($mainEmployeeID);
        }

        Session::flash('message', 'Successfully deleted the employee!');

        return Redirect::to('admin/employees');
    }

    public function saveEmployeetToEmployee(Request $request, $Employee = null)
    {
        EmployeeToEmployee::where('employee_tag_id', '=', $Employee->id)->delete();
        $Employee->employee()->sync($request->get("employee_id"));
    }

    public function getEmployeeRelation($employee_id = null)
    {
        $query = DB::connection('project')
            ->table('employees')
            ->select(
                (
                DB::connection("project")->raw('
												employees.id,
                                                employees_language.title as `title`,
                                                employees_categories_language.title as `category`,
												CASE WHEN (SELECT count(*) FROM employees_to_employees 
															WHERE 
															(employees_to_employees.employee_id = employees.id and employee_tag_id = "' . $employee_id . '") 
															OR 
															(employees_to_employees.employee_tag_id = employees.id and employee_id = "' . $employee_id . '")
														) > 0 THEN 1 ELSE 0 END AS checked
											')
                )
            )->leftjoin('employees_language', 'employees.id', '=', 'employees_language.employee_id')
            ->leftJoin('employees_categories_language', 'employees_categories_language.id', '=', 'employees_language.language_id')
            ->where('employees_language.language_id', '=', '1')
            ->where('employee_id', '<>', $employee_id)
            ->orderBy("employees_language.title");

        return DataTables::queryBuilder($query)
            ->addColumn('radio', function ($model) {
                return '<input type="checkbox" name="employee_id[]" value="' . $model->id . '" ' . ($model->checked == 1 ? 'checked="checked"' : '') . ' id="chkbox_' . $model->id . '" > ';
            })
            ->rawColumns(['radio'])
            ->make(true);
    }
}
