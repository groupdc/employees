<?php

namespace Dcms\Employees\Models;

use Dcms\Core\Models\EloquentDefaults;

class Employee extends EloquentDefaults
{
    protected $connection = 'project';

    public function detail()
    {
        return $this->hasMany('Dcms\Employees\Models\Detail', 'employee_id', 'id');
    }

    public function employee()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('Dcms\Employees\Models\Employee', 'employees_to_employees', 'employee_id', 'employee_tag_id');
    }

    public function category()
    {
        return $this->belongsTo("Dcms\Employees\Models\Category", 'employee_category_id', 'id');
        //return $this->hasOne("Category");
    }

    public function productinformation()
    {
        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        //	return $this->belongsToMany('\Dcweb\Dcms\Models\Products\Information', 'employees_to_products_information_group',  'employee_id','information_id');
    }
}
