<?php

namespace Dcms\Employees\Models;

use Dcms\Core\Models\EloquentDefaults;

class EmployeeToEmployee extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "employees_to_employees";
}
