<?php

namespace Dcms\Employees\Models;

use Dcms\Core\Models\EloquentDefaults;

	class Detail extends EloquentDefaults
	{
		protected $connection = 'project';
		protected $table = "employees_language";
    	protected $fillable = array('language_id', 'employee_id', 'title', 'text', 'description');

		public function employee()
		{
			return $this->belongsTo('Dcms\Employees\Models\Employee','employee_id','id');
		}

		public function employeecategory()
		{
				return $this->belongsTo('Dcms\Employees\Models\Category', 'employee_category_id', 'id');
		}

		public function pages()
		{
			// BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
			return $this->belongsToMany('Dcms\Pages\Models\Pages', 'employees_language_to_pages', 'employee_detail_id', 'page_id');
		}

//		public function products()
//		{
//			// BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
//			return $this->belongsToMany('\Dcweb\Dcms\Models\Products\Information', 'employees_language_to_products_information', 'employees_language_id', 'products_information_id');
//		}
	}
