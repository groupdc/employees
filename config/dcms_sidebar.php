<?php

return [
    "Employees" => [
        "icon"  => "fa-user-friends",
        "links" => [
            ["route" => "admin/employees", "label" => "Employees", "permission" => "employees-browse"],
            ["route" => "admin/employees/categories", "label" => "Categories", "permission" => "employees-browse"],
        ],
    ],
];

?>
